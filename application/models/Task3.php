<?php

	class Application_Model_Task3{

		public function __construct(){
			$this->data_path = './../data/task3/';
		}

		public function getItems(){
			$list = scandir($this->data_path);
			$items = array();

			foreach ($list as $file_name){
				$file_path = $this->data_path.$file_name;
				if (
					is_file($file_path) &&
					strpos($file_name, 'person_') === 0
				){
					$file_data = file_get_contents($file_path);
					if ($file_data){
						$item = json_decode($file_data, true);
						$items[$item['id']] = $item;
					}
				}
			}
			if (count($items)){
				return $items;
			}else{
				return NULL;
			}
		}

		public function getItem($id){
			$file_path = $this->data_path.'person_'.$id.'.json';
			if (is_file($file_path)){
				$file_data = file_get_contents($file_path);
				if (!empty($file_data)){
					$file_data = json_decode($file_data, true);
					if (is_array($file_data)){
						return $file_data;
					}
				}
			}
			return NULL;
		}
		
		public function save($data, $id = null){
			if (!empty($data)){
				if (!$id){
					$id = count(scandir($this->data_path)) - 2;
				}
				$data['id'] = $id;
				$file_name = 'person_'.$id.'.json';
				$file_data = json_encode($data, JSON_UNESCAPED_UNICODE);
				file_put_contents($this->data_path.$file_name, $file_data);
				return true;
			}
			return false;
		}

	}