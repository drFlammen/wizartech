<?php

class Task1Controller extends Zend_Controller_Action
{

    public function indexAction(){
        $form1 = new Application_Form_Task1_Form1();
        $this->view->form1 = $form1;

        $form2 = new Application_Form_Task1_Form2();
        $this->view->form2 = $form2;
    }

}

