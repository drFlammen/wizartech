<?php

class Task3Controller extends Zend_Controller_Action
{

	public function init(){
		$this->_helper->redirector = $this->_helper->getHelper('UpdatedRedirector');
	}

    public function indexAction(){
        $model = new Application_Model_Task3();
        $items = $model->getItems();
        $this->view->items = $items;
    }

    public function addAction(){
        $post = $this->_request->getPost();
        $form = new Application_Form_Task2_Form1();

        if ($post && $form->isValid($post)){
            $model = new Application_Model_Task3();

            $model->save($form->getValues());
            $this->redirect('/task3');
        }else{
            $form->setDefaults($post);
        }
        $this->view->form = $form;
    }

	public function editAction(){
		if ($this->_request->has('id')){
			$id = $this->_request->getParam('id');
			$model = new Application_Model_Task3();
			$item = $model->getItem($id);
			if ($item){
				$post = $this->_request->getPost();
				$form = new Application_Form_Task2_Form1();

				if ($post && $form->isValid($post)){
					$model->save($form->getValues(), $id);
					$this->redirect('/task3');
				}else{
					$form->setDefaults($item);
					if ($post){
						$form->setDefaults($post);
					}
					$this->view->form = $form;
				}
			}else{
				$this->redirect('/task3');
			}
		}else{
			$this->redirect('/task3');
		}
	}
}

