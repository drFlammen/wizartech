<?php

	class Task7Controller extends Zend_Controller_Action
	{

		public function indexAction(){
			$post = $this->_request->getPost();
			$form = new Application_Form_Task7_Form1();

			if ($post && $form->isValid($post)){
			}else{
				$form->setDefaults($post);
			}

			$this->view->form = $form;
		}

		public function metroAction(){
			$this->getHelper('viewRenderer')
				->setNoRender(true);
			$this->_helper->layout->disableLayout();
			$this->getResponse()
				->setHeader('Content-type', 'text/json; charset=UTF-8');

			$resp = '{}';
			if ($this->_request->has('city')){
				$city = $this->_request->getParam('city');

				$file_path = './js/task6/'.$city.'.json';
				if (is_file($file_path)){
					$file_data = file_get_contents($file_path);
					if (strlen($file_data)){
						$resp = $file_data;
					}
				}
			}
			echo $resp;
		}

	}

