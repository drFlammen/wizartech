<?php

	class Zend_Validate_CustomPhone extends Zend_Validate_Abstract {

		private $country_code_len = 2;
		private $city_code_len = 4;
		private $phone_number_len = 8;
		private $additional_number_len = 4;

		protected $_messageTemplates = array ();

		public function __construct(){
			$this->setMessageTemplates();
		}

		protected function setMessageTemplates(){
			$this->_messageTemplates = array(
				'country_code_len' => 'Код страны дожен быть не более '.$this->country_code_len.' символов',
				'city_code_len' => 'Код города дожен быть не более '.$this->city_code_len.' символов',
				'phone_number_len' => 'Номер телефона дожен быть не более '.$this->phone_number_len.' символов',
				'additional_number_len' => 'Доп.номер дожен быть не более '.$this->additional_number_len.' символов',

				'country_code_letter' => 'Код страны дожен состоять только из цифр',
				'city_code_letter' => 'Код города дожен состоять только из цифр',
				'phone_number_letter' => 'Номер телефона дожен состоять только из цифр',
				'additional_number_letter' => 'Доп.номер дожен состоять только из цифр',
			);
		}

		public function isValid($value, $context = null) {
			$result = true;
			foreach ($value as $field_name => $field_value){
				if ($field_value === "") continue;

				$letters = preg_match('/^\d/', $field_value);
				if (!$letters){
					$error_field_name = $field_name.'_letter';
					$this->_error($error_field_name);
					$result = false;
				}

				$field_length_name = $field_name.'_len';
				$field_length = $this->$field_length_name;
				if (mb_strlen($field_value, 'UTF-8') > $field_length){
					$this->_error($field_length_name);
					$result = false;
				}
			}

			return $result;
		}
	}
