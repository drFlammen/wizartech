<?php

	class Zend_View_Helper_FormPhone extends Zend_View_Helper_FormElement
	{

		public function formPhone($name, $value, $attribs = null)
		{

			$helper = new Zend_View_Helper_FormText();
			$helper->setView($this->view);

			$xhtml = '';

			$xhtml .= '+ '.$helper->formText($name.'[country_code]', $value['country_code'], array('class' => 'country_code', 'data-dojo-type' => 'dijit/form/TextBox'));
			$xhtml .= ' ( '.$helper->formText($name.'[city_code]', $value['city_code'], array('class' => 'city_code', 'data-dojo-type' => 'dijit/form/TextBox')).' ) ';
			$xhtml .= $helper->formText($name.'[phone_number]', $value['phone_number'], array('class' => 'phone_number', 'data-dojo-type' => 'dijit/form/TextBox'));
			$xhtml .= ' (доп. '.$helper->formText($name.'[additional_number]', $value['additional_number'], array('class' => 'additional_number', 'data-dojo-type' => 'dijit/form/TextBox')).' )';

			return $xhtml;
		}
	}
