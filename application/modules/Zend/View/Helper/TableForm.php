<?php

	class Zend_View_Helper_TableForm extends Zend_View_Helper_Form
	{

		public function tableForm($name, $attribs = null, $content = false)
		{
			$info = $this->_getInfo($name, $content, $attribs);
			extract($info);

			if (!empty($id)) {
				$id = ' id="' . $this->view->escape($id) . '"';
			} else {
				$id = '';
			}

			if (array_key_exists('id', $attribs) && empty($attribs['id'])) {
				unset($attribs['id']);
			}

			if (!empty($name) && !($this->_isXhtml() && $this->_isStrictDoctype())) {
				$name = ' name="' . $this->view->escape($name) . '"';
			} else {
				$name = '';
			}

			if ($this->_isHtml5() && array_key_exists('action', $attribs) && !$attribs['action']) {
				unset($attribs['action']);
			}

			if ( array_key_exists('name', $attribs) && empty($attribs['id'])) {
				unset($attribs['id']);
			}

			$xhtml = '<form'
				. $id
				. $name
				. $this->_htmlAttribs($attribs)
				. '><table id="form-table-'.$id.'">';

			if (false !== $content) {
				$xhtml .= $content
					.  '</table></form>';
			}

			return $xhtml;
		}
	}
