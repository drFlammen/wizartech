<?php

	class Zend_Controller_Action_Helper_UpdatedRedirector extends Zend_Controller_Action_Helper_Redirector{

		public function redirectAndExit(){
			$front = Zend_Controller_Front::getInstance();
			$plugin = $front->getPlugin(Zend_Controller_Plugin_UserLogger)->dispatchLoopShutdown();

			parent::redirectAndExit();
		}
	}