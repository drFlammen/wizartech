<?php

	class Zend_Controller_Plugin_UserLogger extends Zend_Controller_Plugin_Abstract{

		private $start_time = null;
		private $end_time = null;
		private $post = array();

		public function __construct(){
			$this->start_time = time()+microtime();
		}

		public function preDispatch(Zend_Controller_Request_Abstract $request)
		{
			$this->post = $this->getRequest()->getPost();
		}

		public function dispatchLoopShutdown(){
			$this->end_time = time()+microtime();

			$request = $this->getRequest();
			$post = $request->getPost();

			$log_params = array(
				'start_time' => date('[d.m.Y H:i]', $this->start_time),
				'query_time' => ($this->end_time - $this->start_time).'s',
				'ip_address' => $_SERVER['REMOTE_ADDR'],
				'request_uri' => $_SERVER['REQUEST_URI'],
			);
			if (!empty($this->post)){
				$log_params['post'] = json_encode($this->post, JSON_UNESCAPED_UNICODE);
			}
//			Zend_Debug::dump($request->isPost());
//			Zend_Debug::dump($log_params);
//			die();
			$this->writeLog($log_params);
		}

		private function writeLog($params){
			$file_path = './../data/task7/'.date('d.m.Y').'.log';
			$fh = fopen($file_path, 'a+');
			fwrite($fh, implode("\t\t", $params)."\r\n");
			fclose($fh);
		}

	}