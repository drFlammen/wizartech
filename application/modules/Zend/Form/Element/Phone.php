<?php

	require_once 'Zend/Form/Element/Xhtml.php';

	class Zend_Form_Element_Phone extends Zend_Form_Element_Xhtml{
		public $helper = 'formPhone';

		public function setValue($value){

			foreach ($value as $key => $val){
				if (empty($val)){
					unset($value[$key]);
				}
			}

			$this->_value = $value;
			return $this;
		}
	}
