<?php

	class Zend_Form_Decorator_TrTdWrapper extends Zend_Form_Decorator_Abstract{

		public function render($content){
			$element = $this->getElement();
			$elementName = $element->getName();
			$elementLabel = $element->getLabel();
			$elementReq = $element->isRequired();

			$xhtml = '<tr>';
			if (!empty($elementLabel)){

				if ($elementReq){
					$elementLabel .= ' *';
				}

				$xhtml .= '<td id="'.$elementName.'-label">'.$elementLabel.'</td>'.
					'<td id="'.$elementName.'-element">'.$content.'</td>';
			}else{
				$xhtml .= '<td colspan="2" id="'.$elementName.'-element">'.$content.'</td>';
			}
			$xhtml .= '</tr>';

			return $xhtml;
		}
	}
