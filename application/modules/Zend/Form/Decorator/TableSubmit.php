<?php

	class Zend_Form_Decorator_TableSubmit extends Zend_Form_Decorator_Abstract{

		public function render($content){
			$element = $this->getElement();
			$elementName = $element->getName();
			$elementLabel = $element->getLabel();

			$xhtml = '<tr><td colspan="2"><input type="submit" name="'.$elementName.'" value="'.$elementLabel.'" id="'.$elementName.'" /></td></tr>';
			return $xhtml;
		}
	}
