<?php

	class Application_Form_Task1_Form2 extends Zend_Form{

		public function init(){
			$this->setMethod('post');
			$this->setDecorators(array(
				'FormElements',
				'TableForm',
			));

			$this->setElementDecorators(
				array(
					array('ViewHelper'),
					array('TrTdWrapper'),
				)
			);

			$this->addElement('text', 'company', array(
					'label' => 'Название компании',
					'required' => true
				))
				->addElement('text', 'address', array(
					'label' => 'Юридический адрес'
				))
				->addElement('text', 'account_num', array(
					'label' => 'Расчетный счет'
				))

				->addElement('submit', 'submit', array('label' => 'Отправить', 'decorators' => array('TableSubmit')));
		}

	}
