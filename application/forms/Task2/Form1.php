<?php

	class Application_Form_Task2_Form1 extends Zend_Form{

		public function init(){
			$this->setMethod('post');
			$this->setDecorators(array(
				'FormElements',
				'TableForm',
			));

			$this->setElementDecorators(
				array(
					array('ViewHelper'),
					array('Errors'),
					array('TrTdWrapper'),
				)
			);

			$this->addElement('text', 'name', array(
				'label' => 'Имя',
				'required' => true))
				->addElement('text', 'first_name', array(
					'label' => 'Фамилия'))
				->addElement('select', 'city', array(
					'label' =>  'Город',
					'multiOptions' => $this->getCitiesNodes()))
				->addElement('textarea', 'comment', array(
					'cols' => 65,
					'rows' => 5))
				->addElement('phone', 'phone', array(
					'label' => 'Телефон',
					'validators' => array('CustomPhone')
				));

			$this->addDisplayGroup(
				array('name', 'first_name', 'city', 'phone'),
				'main_info',
				array(
					'legend' => 'Основная информация',
					'decorators' => array('FormElements', 'TableFieldset')
				)
			);
			$this->addDisplayGroup(
				array('comment'),
				'other_info',
				array(
					'legend' => 'Другая информация',
					'decorators' => array('FormElements', 'TableFieldset')
				)
			);

			$this->addElement('submit', 'submit', array('label' => 'Отправить', 'decorators' => array('TableSubmit')));

			$this->getElement('phone')->setAllowEmpty(true);
		}

		public function getCitiesNodes(){
			return array(
				'spb' => 'Санкт-Петербург',
				'msk' => 'Москва',
				'rzn' => 'Рязань',
			);
		}
	}
