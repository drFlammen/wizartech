<?php

	class Application_Form_Task7_Form1 extends Zend_Form{

		public function init(){
			$this->setMethod('post');
			$this->setDecorators(array(
				'FormElements',
				'TableForm',
			));
			$this->setAttrib('id', 'validation_form');

			$this->setElementDecorators(
				array(
					array('ViewHelper'),
					array('Errors'),
					array('TrTdWrapper'),
				)
			);

			$this->addElement('text', 'name', array(
					'label' => 'Имя',
					'required' => true,
					'data-dojo-type' => 'dijit/form/ValidationTextBox',
				))
				->addElement('text', 'first_name', array(
					'label' => 'Фамилия',
					'data-dojo-type' => 'dijit/form/ValidationTextBox',
				))

				->addElement('select', 'city', array(
					'label' =>  'Город',
					'multiOptions' => $this->getCitiesNodes(),
					'data-dojo-type' => 'dijit/form/Select',
					'data-dojo-props' => '
						onChange: updateStorage,
					'
				))
				->addElement('select', 'metro', array(
					'label' => 'Метро',
					'multiOptions' => array(),
					'data-dojo-type' => 'dijit/form/Select',
					'data-dojo-props' => '
						store: metroObserver,
						labelAttr: "title"
					',
				))
				->addElement('textarea', 'comment', array(
					'cols' => 65,
					'rows' => 5,
					'data-dojo-type' => 'dijit/form/SimpleTextarea'
				))
				->addElement('phone', 'phone', array(
					'label' => 'Телефон',
					'validators' => array('CustomPhone')
				));

			$this->addDisplayGroup(
				array('name', 'first_name', 'city', 'metro', 'phone'),
				'main_info',
				array(
					'legend' => 'Основная информация',
					'decorators' => array('FormElements', 'TableFieldset')
				)
			);
			$this->addDisplayGroup(
				array('comment'),
				'other_info',
				array(
					'legend' => 'Другая информация',
					'decorators' => array('FormElements', 'TableFieldset')
				)
			);

			$this->addElement('button', 'submit', array(
					'label' => '',
					//'decorators' => array('TableSubmit'),
					'data-dojo-type' => 'dijit/form/Button',
					'data-dojo-props' => '
						id: "submit",
						label: "Отправить",
					'
				));

			$this->getElement('phone')->setAllowEmpty(true);
			$this->getElement('metro')->setAllowEmpty(true);
		}

		public function getCitiesNodes(){
			return array(
				'rzn' => 'Рязань',
				'spb' => 'Санкт-Петербург',
				'msk' => 'Москва',
			);
		}
	}
