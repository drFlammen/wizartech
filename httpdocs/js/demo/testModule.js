define (
	[
		'dojo/dom',
		'dojo/dom-construct',
		'dojo/fx',
		'dojo/_base/fx',
		'dojo/query',
		'dojo/_base/array',
		'dojo/request',
		'dojo/NodeList-dom'
	],
	function (dom, domConstruct, fx, baseFx, query, array, request){

		var $ul = query('.test_ul')[0];
		fx.chain([
			fx.wipeIn({node: $ul, duration: 2000}),
			baseFx.fadeOut({node: $ul, duration: 2000})
		]).play();


		$tr = query('#table tr:nth-child(odd) td');
		$tr.style('background', '#bbbbbb');
		$tr.forEach(function($td, index){
			$td.innerText = 'td'+index;
		});



		return {
			setText: function(){
				var node = dom.byId('text');
				node.innerHTML = '<em>qweqweqweqweqweqweqwe</em><br /><strong>asddsadadadsadad</strong><br />zxcxzcxzczxczx';
			},
			moveText: function(){
				var node = dom.byId('text');

				var top = Math.floor(100 + Math.random() * 200);
				var left = Math.floor(100 + Math.random() * 200);

				fx.slideTo({
					node: node,
					top: top,
					left: left
				}).play();
			},
			getAjaxData: function(){
				request("/dojo/ajax_data.json", {handleAs: "json"}).then(
					function (data){
						var $text = dom.byId('text');
						domConstruct.place('<p>'+data.text+'</p>', $text, 'before');

						var $h1 = dom.byId('greeting');
						domConstruct.place('<h1>'+data.data.title+'</h1>', $h1, 'replace');
					},
					function (err){
						console.log(err);
					}
				);
			}
		}
	}
);